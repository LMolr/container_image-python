ARG BASE_IMAGE='python'
ARG BASE_IMAGE_TAG='3.10-slim'
FROM $BASE_IMAGE:$BASE_IMAGE_TAG

LABEL maintainer="Luca Molari <molari.luca@gmail.com>"

ARG CONTAINER_USER_UID=1000
ARG CONTAINER_USER_NAME='container_user'
ARG CONTAINER_USER_GID=1000
ARG CONTAINER_USER_GROUP='container_group'
ARG PIP_TIMEOUT=1000

ENV CONTAINER_USER_UID="${CONTAINER_USER_UID}"
ENV CONTAINER_USER_NAME="${CONTAINER_USER_NAME}"
ENV CONTAINER_USER_GID="${CONTAINER_USER_GID}"
ENV CONTAINER_USER_GROUP="${CONTAINER_USER_GROUP}"
ENV PIP_TIMEOUT="${PIP_TIMEOUT}"
ENV PYTHONPATH="/srv/main/lib/:${PYTHONPATH}"
ENV PATH="/home/${CONTAINER_USER_NAME}/.local/bin:/srv/bin:${PATH}"

SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

WORKDIR '/srv/'

CMD '/srv/bin/launch.sh'

##########################################################################################

USER 0
RUN set -x; \
  id

RUN set -x; \
  groupadd "${CONTAINER_USER_GROUP}" -g "${CONTAINER_USER_GID}" ; \
  useradd "${CONTAINER_USER_NAME}" -u "${CONTAINER_USER_UID}" -g "${CONTAINER_USER_GID}" -m -s '/bin/bash'

RUN set -x; \
  mkdir -p '/srv/bin/' ; \
  mkdir -p '/srv/infra/lib/' ; \
  mkdir -p '/srv/main/assets/' ; \
  mkdir -p '/srv/main/config/plain/' ; \
  mkdir -p '/srv/main/config/secret/' ; \
  mkdir -p '/srv/main/lib/' ; \
  mkdir -p '/srv/test/config/plain/' ; \
  mkdir -p '/srv/test/config/secret/' ; \
  mkdir -p '/srv/test/lib/' ; \
  mkdir -p '/srv/test/resources/' ; \
  mkdir -p '/srv/run/log/' ; \
  mkdir -p '/srv/run/shared/' ; \
  chown -R "${CONTAINER_USER_NAME}:${CONTAINER_USER_GROUP}" '/srv'

RUN set -x; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
      'curl' \
    ; \
    apt-get clean; \
    apt-get autoclean; \
    apt-get autoremove; \
    rm -rf /var/lib/apt/lists/*

RUN set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    --upgrade pip

COPY './docker/python-3.10/python-3.10.requirements.txt' '/srv/infra/lib/'

RUN set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    -r '/srv/infra/lib/python-3.10.requirements.txt'

RUN set -x; \
  pip freeze --all; \
  pip check

COPY './docker/python-3.10/bin/' '/srv/bin/'

RUN set -x; \
  chown -R "${CONTAINER_USER_NAME}:${CONTAINER_USER_GROUP}" '/srv'; \
  chmod +x /srv/bin/*
  
##########################################################################################

USER "${CONTAINER_USER_NAME}"
RUN set -x; \
  id

RUN set -x; \
  mkdir -p "/home/${CONTAINER_USER_NAME}/.local/bin/"
