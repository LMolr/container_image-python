ARG BASE_IMAGE='registry.gitlab.com/lmolr/container_image-python/python-3.7'
ARG BASE_IMAGE_TAG='dev-latest'
FROM $BASE_IMAGE:$BASE_IMAGE_TAG
FROM $BASE_IMAGE:$BASE_IMAGE_TAG

LABEL maintainer="Luca Molari <molari.luca@gmail.com>"

ARG PIP_TIMEOUT=1000

ENV PIP_TIMEOUT="${PIP_TIMEOUT}"

SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

USER "${DOCKER_USER_NAME}"
RUN id

COPY './docker/python-3.7-ai/python-3.7-ai.requirements.txt' '/srv/lib/'

RUN set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    -r '/srv/lib/python-3.7-ai.requirements.txt'

RUN set -x; \
  pip freeze --all; \
  pip check
