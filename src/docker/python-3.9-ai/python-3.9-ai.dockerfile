ARG BASE_IMAGE='registry.gitlab.com/lmolr/container_image-python/python-3.9'
ARG BASE_IMAGE_TAG='dev-latest'
FROM $BASE_IMAGE:$BASE_IMAGE_TAG
FROM $BASE_IMAGE:$BASE_IMAGE_TAG

LABEL maintainer="Luca Molari <molari.luca@gmail.com>"

ARG PIP_TIMEOUT=1000

ENV PIP_TIMEOUT="${PIP_TIMEOUT}"

SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

##########################################################################################

USER "${CONTAINER_USER_NAME}"
RUN set -x; \
  id

COPY './docker/python-3.9-ai/python-3.9-ai.requirements.txt' '/srv/infra/lib/'

RUN set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    -r '/srv/infra/lib/python-3.9-ai.requirements.txt'

RUN set -x; \
  pip freeze --all; \
  pip check
