ARG BASE_IMAGE='python'
ARG BASE_IMAGE_TAG='3.7-slim'
FROM $BASE_IMAGE:$BASE_IMAGE_TAG

LABEL maintainer="Luca Molari <molari.luca@gmail.com>"

ARG DOCKER_USER_UID=1000
ARG DOCKER_USER_NAME='docker_user'
ARG DOCKER_USER_GID=1000
ARG DOCKER_USER_GROUP='docker_group'
ARG PIP_TIMEOUT=1000

ENV DOCKER_USER_UID="${DOCKER_USER_UID}"
ENV DOCKER_USER_NAME="${DOCKER_USER_NAME}"
ENV DOCKER_USER_GID="${DOCKER_USER_GID}"
ENV DOCKER_USER_GROUP="${DOCKER_USER_GROUP}"
ENV PIP_TIMEOUT="${PIP_TIMEOUT}"
ENV PYTHONPATH="/srv/lib/:${PYTHONPATH}"

SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

USER 0
RUN id

RUN set -x; \
  groupadd "${DOCKER_USER_GROUP}" -g "${DOCKER_USER_GID}" ; \
  useradd "${DOCKER_USER_NAME}" -u "${DOCKER_USER_UID}" -g "${DOCKER_USER_GID}" -m -s '/bin/bash'

RUN set -x; \
  mkdir -p '/srv/assets/' ; \
  mkdir -p '/srv/bin/' ; \
  mkdir -p '/srv/cfg/' ; \
  mkdir -p '/srv/lib/' ; \
  mkdir -p '/srv/run/' ; \
  mkdir -p '/srv/run/log/' ; \
  mkdir -p '/srv/run/shared/' ; \
  mkdir -p '/srv/secrets/' ; \
  chown -R "${DOCKER_USER_NAME}:${DOCKER_USER_GROUP}" '/srv'

RUN set -x; \
  pip install --no-cache-dir \
    --default-timeout="${PIP_TIMEOUT}" \
    --upgrade pip

RUN set -x; \
  pip freeze --all; \
  pip check

USER "${DOCKER_USER_NAME}"
RUN id

RUN set -x; \
  mkdir -p "/home/${DOCKER_USER_NAME}/.local/bin/"

ENV PATH="/home/${DOCKER_USER_NAME}/.local/bin:/srv/bin:${PATH}"

WORKDIR '/srv/'
