#!/usr/bin/env bash

set -euo pipefail;

echo 'Sleeping forever ...'

set -x;

sleep infinity
